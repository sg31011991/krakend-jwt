const mongoose = require('mongoose')

mongoose.connect('mongodb://34.125.228.14:27017/auth-demo-db', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log('mongodb connected.')
    })
    .catch((err) => console.log(err.message))

mongoose.connection.on('connected', () => {
    console.log('Mongoose connected to db')
})

mongoose.connection.on('error', (err) => {
    console.log(err.message)
})

mongoose.connection.on('disconnected', () => {
    console.log('Mongoose connection is disconnected.')
})

process.on('SIGINT', async() => {
    await mongoose.connection.close()
    process.exit(0)
})
