const express = require('express')
const router = require('express').Router();
const products = require('../controllers/products')

router.get("/", products.getAllProducts);
router.get("/:name", products.getAllProductsTesting);


module.exports = router;
