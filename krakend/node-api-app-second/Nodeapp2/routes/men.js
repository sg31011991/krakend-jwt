const express = require("express");
const router = new express.Router();
const User = require("../src/models/registers")

// We will handel get request API
router.get("/registers", async(req, res) => {
    try {
        const getregisters = await User.find({});
        res.send(getregisters);
    } catch (error) {
        res.status(400).send(e);
    }
})

router.get("/registers/:userid", async(req, res) => {
    try {
        const userid = req.params.userid
        const registersdata = await User.findOne({ userid: req.params.userid });
        console.log(registersdata);
        if (registersdata) {
            res.send(registersdata)
        } else {
            return res.status(404).send();
        }


    } catch (e) {
        res.send(e);
    }
})

// router.get("/registers/:id", async(req, res) => {
//     try {
//         const _id = req.params.id
//         const registersid = await User.findOne({ id: req.params.id });
//         console.log(registersid);
//         if (registersid) {
//             res.send(registersid)
//         } else {
//             return res.status(404).send();
//         }


//     } catch (e) {
//         res.send(e);
//     }
// })



module.exports = router;
