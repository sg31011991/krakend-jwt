const mongoose = require("mongoose");

mongoose.set('strictQuery', true);
mongoose.connect('mongodb://34.125.87.102:27017/demo-db', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log('MongoDB Connected...'))
    .catch((err) => console.log(err))
